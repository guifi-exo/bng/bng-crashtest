# Overview
This repository contains a couple of simple shell scripts to compile and install Accel-PPP from [Bng-debug](https://gitlab.com/guifi-exo/bng/bng-debug) in a Debian system. They also invokes `gdb` (GNU Debugger) in order to be attached to a running Accel-PPP daemon.

# Installation
Go to server's directory `/opt` and clone this repository this way:
```
# git clone https://gitlab.com/guifi-exo/bng/bng-crashtest.git .
```
Be sure the current directory is empty before to clone it. Next, enable the executing rights of the scripts. So simple.

# Running
To build and install a fresh binary of Accel-PPP, simply go to `/opt` and run:
```
# ./install-accelppp-git-exo
```
You can also change the built branch or git head modifying the variable `git_branch` in shell script code.
This script installs a tool named `show-active` in /root directory. If you want to show all the active connections invoke:
```
./show-active
 ifname  |      username      |  called-sid   | calling-sid |       ip       |          ip6-dp          | type | state  |  uptime  
---------+--------------------+---------------+-------------+----------------+--------------------------+------+--------+----------
 i0082c0 | jmercader@exo.cat  | 10.38.140.244 | 10.1.56.209 | 45.150.184.137 | 2a0f:de00:fe00:5200::/56 | l2tp | active | 01:29:46
 i0081c0 | lafundicio@exo.cat | 10.38.140.244 | 10.1.56.210 | 45.150.184.136 | 2a0f:de00:fe00:5100::/56 | l2tp | active | 01:29:46
 i0043c0 | jmoles@exo.cat     | 10.38.140.244 | 10.1.56.212 | 45.150.184.113 | 2a0f:de00:fe00:2b00::/56 | l2tp | active | 01:29:46
```
To show just the count of L2TP and PPPoE connections, type:
```
./show-active -c
PPPoE:           0
L2TP:            3
Management:      0
User:            3
Total:           3
```


To attach `gdb` to a running Accel-PPP daemon, create a new `tmux` session with some name (`<session-name>`):
```
# tmux new -s <session-name>
```
Now run the script and detach the created tmux session by pressing `Ctrl+b+d`:
```
# ./attach_gdb_to_accelppp
```
You can check the list of created sessions invoking this:
```
# tmux ls
```
And re-attach to `<session-name>` invoking:
```
# tmux attach-session -t <session-name>
```
If Accel-PPP daemon finishes for any reason, `gdb` will create a trace and eventually a coredump. Change the name and location of these files simply editing the source code of this shell script.
